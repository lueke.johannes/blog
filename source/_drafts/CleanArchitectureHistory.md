---
title: History - 'Clean Architecture'
toc: true
cover: 
thumbnail: 
date: 2020-10-20 16:42:06
excerpt: Short review of the development of clean architecture in code
tags:
- Clean Architecture
- Onion Architecture
- Domain Driven Design
- DDD
categories:
- TodoApp
---


## Origins

The term "Clean Architecture" was coined by Robert C. Martin, widley known as Uncle Bob. With him "Clean Architecture" as a term took of like a rocket. But the Idea behind it started far earlier.

### History of "Clean Architecture"

|Date|Architecture Model| Author | Blog |Books |
|----|------------------|--------|------|------|
|1996|BCE| Ivar Jacobson|-|[Amazon-Affiliate Link to book](https://amzn.to/3k9UatR)|
|2005|Hexagonal Architecture|Alistair Cockburn|[more](http://www.dossier-andreas.net/software_architecture/ports_and_adapters.html)/[more](http://wiki.c2.com/?PortsAndAdaptersArchitecture)|[Amazon-Affiliate Link to book](https://amzn.to/35djF7l)|
|2008|Onion Architecture| Jeffrey Palermo|[more](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/)||
|2010|DCI| James Coplien, Trygve Reenskaug|-|[Amazon-Affiliate Link to book](https://amzn.to/3o5zFAI)|
|2012|Clean Architecture|Robert C. Martin|[more](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)|[Amazon-Affiliate Link to book](https://amzn.to/3jajvlZ)|

#### UseCase driven approach

In 1996 Ivar Jacobson introduced the Idea of the UseCase driven approach for OOP.

placeholder

#### Hexagonal Architecture or Ports and Adapters

Alistair Cockburn tried to achieve the following with its architecture:

> "Allow an application to equally be driven by users, programs, automated test or batch scripts, and to be developed and tested in isolation from its eventual run-time devices and databases."

This design is recommend if you whish for high unit-test code coverage and do like the ability to quickly switch between any type of third-party code of a certain functionality.

This so called "certain functionality" are seperated from the main code by defining **ports** which decouples the **Core Logic** from any third-party code necessary. Through this decoupling you are allowing your application to be driven by not just your user but any entity which wants to use your **Core Logic**. Within the **Core Logic** you might find hidden layers and their dependencies which might flow outwards. But these outwards depedencies will end at the **ports**.

If you comapre the **hexagonal architecture** with the **layered architecture** you can see that the **ports** cleary decouple the logic from anything else while the layered approach "daisy chains" its dependencies from bottom to top, any change in the chain might effect other layers directly.

Where as any change within the **Core Logic** of the hexagonal architecture is hidden behind the ports and vice versa any changes within **Adapters** are hidden behind same **ports** as well. In other words: The **Core Logic** knows the "outside" world only through its **ports**. If you look carefully you can see the dependencies flow inwards to the **Core Logic**.

Which fits my description of clean architecture from the beginning.

![Hexagonal Architecture Vs Layered Architecture. Image by Stephan Schneider](https://www.maibornwolff.de/sites/default/files/1_layered-architecture_vs_ports-and-adapters.png)

If you ever find an outwards flowing dependency within an **hexagonal architecture** which crosses the **ports** layer then you found a bug!

#### Onion Architecture

The **onion architecture** is very similiar compared to the **hexagonal architecture**. Both isolate any external dependencies away from the actual buisness logic. Note worthy is that the onion architecture does it one way only compared to the two way isolation of the **hexagonal architecture**. This means the outer most layers of the onion can access any inner layer but no inner layer will every have any outwards flowing dependencies.

{% image /assets/todolist-onionArchitecture.PNG 'Onion Architecture by Jeffrey Palermo. Image by Johannes Lüke'  %}

> "With traditionally layered architecture, a layer can only call the layer directly beneath it. This is one of the key points that makes **Onion Architecture** different from traditional layered architecture." – Palermo

Again here as well my description of **clean architecture** from the beginning does fit.

If you ever find an outwards flowing dependency within an **onion architecture** then you found a bug!

#### DCI

placeholder

#### Clean Architecture

The design by Robert C. Martin is some what the cumulation of the previous touched approachs of clean architecture. It combines concepts to create new way of clean architecture.

|Clean Archtiecture|Onion Architecture| Hexagonal Architecture|
|------------------|------------------|-----------------------|
| Entities | domain-models | Core Logic |
| Entities | domain-services | Core Logic |
| Use Cases | application-services | Core Logic |
| Interface Adapters - Controllers | - | Port |
| Interface Adapters - Gateways | - | Port |
| Interface Adapters - Presenters | - | Port |
| FrameWorks & Drivers - UI| Presentation | Adapter |
| FrameWorks & Drivers - DB| Infrastructure | Adapter |
| FrameWorks & Drivers - external | Infrastructure | Adapter |
| - | Tests | - |
Mapping

![The Clean Architecture by Robert C. Martin](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)
---
title: The Architecture - 'How is the code organized'
toc: true
cover: /assets/todolist-onionArchitecture.PNG
thumbnail: /assets/todolist-onionArchitecture.PNG
date: 2020-10-20 16:42:06
excerpt: Thoughts about the way the application should be organized
tags:
- Clean Architecture
- Onion Architecture
- Domain Driven Design
- DDD
categories:
- TodoApp
---

In the first post of this series i already have touched this topic, i did loosly summarize what i understood is clean architecture:

> The Concept of "Clean Architecture" is quite simple. In essence every outerlayer of an software architecture can use parts of the innerlayers beneath it but never the otherway around.

This statement was true than and is still true now. But clean architecture is more then this dependency rule. This post tries first to establish a common understanding of what "Clean Architecture" is and then how i intend to apply it to the todo-app project.

<!-- more -->

### Clean architecture properties

All approaches have one thing in common they represent concretric architecture with very strong emphasis on **seperation of concerns**. This enables the programmer to easily modify or extend a system developed with one of these approaches as well as to support practices like **TDD** more easily.

Another commonality is that these approaches (except BCE) started to put the logic of ones system into the foreground. Alistair Cockburn called this part of its "Hexagonal Architecture" the **Core Logic** where external dependencies are abstracted away. Or in other terms Databases or UI are degraded to mere implementation details and the business logic is promoted to a VIP within the architecture.

Lets summarize the properties of clean architecture:

- **Directed Dependency** => source code dependencies can only point inwards [Robert C. Martin]
- Emphasis on **Seperation of Concerns** => easy modification and extension of the system.
- Business Logic being the VIP of the architecture => main focus on the buisness Logic, everything non-specific to the application is represented by some form of abstraction

### Consequences for development

- It becomes easier **to develop** components of the system indenpendetly from each other.
- It becomes easier **to test** components of the system independently from each other.
- It becomes easier **to modify** components of the system without affecting other components.
- It becomes easier **to extend** components of the system without affecting other components.
- Any changes on external dependencies will not affect the core implementation
- Software porting becomes a lot easier because only external dependencies must change

### Short Examples

E.g. Imagine you wrote Desktop-Programm with 100k LOC which utilises WPF as UI. Years later Your client comes to you and asks if you could write a Linux variant using Qt(ignoring the fact Qt is platform independed). If you have used a clean architecture approach the only thing you need to reimplement would be the UI. everything else stays the same.

Another Example. Your Application uses MySQL as underlying DB. Suddenly at the end of development your client/stakeholder changes the requirements and you need use suddenly SQLite as DB because for some security reasons. The only thing you need to replace would be the implementation details handling SQLite. Everything else in the Application did atmost knew only some kind of interface which enabled it to read and write data. You could even reuse your test to verify proper functionality.

### Further resources

Ian Cooper did pretty good job talking about these different approaches of the same idea in his DevTernity talk from 2019.

{% youtuber video SxJPQ5qXisw %}
  privacy_mode: yes
{% endyoutuber %}

What i want to focus on is the **Onion Architecture** which i intend to adapt for this project but briefly explain the similarities/differences between Palermos' vs Uncle Bob's approach

## Similarities


{% img  /assets/todolist-architecture.PNG architecture %}
Test

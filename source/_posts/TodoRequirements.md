---
title: The Requirements - "What are the capabilities of the app"
toc: true
thumbnail: /assets/TodoAppMockUp.PNG
tags:
  - Requirements
categories:
  - TodoApp
date: 2020-10-19 16:42:06
---


## Purpose

In my previous post i explained why i am starting this project. In this post i like to explain what the requirements are to consider this project done. In a short sentenced in last post i already stated that this project tries to implement a todo app. A fairly typical "hello world" application in the web development field i would say. Today i like to define the features such an app must have to be considered done.

<!-- more -->

## The necessary core features - Minimal viable product

The app needs certain features to be considered a todo app. But what are theses features? First you need to be aware that we are essentially dealing with a list. The app needs therefore the ability to modify a list of elements where the elements represents a **todo** that a user wants to track. What are the basic operation any List must have? lets list them:

- add a single element
- remove a single element
- change the content of an element

these three basic operation are enough to derive the necessary feature set for the todo app. Are these then the minimal requirements to consider the app done? not quite, if the app is capable of these three operations will still need a way to interact with the user.

Okay with that in mind lets try to define what the app enable the user to do:

 1. Show a list of todo elements in a list like manner, hence a todo-list
 2. Allow the user to add **todos** to the todo-list
 3. Allow the user to Edit **todos** present on the todo-list
 4. Allow the user to mark **todos** as done on the todo-list
 5. Allow the user to delete **todos** on the todo-list

When the app fulfills these 5 requirements we can say with confidence the project is done. But wait, why do we have now 5 requirements and not 4? didn't we establish the three basic list operation are enough and we were only missing a way to interact with the user?

Well, thats correct. What happen is while thinking about user interaction i realised a user like to have the ability to mark todos as done and not simply delete them if the task has been executed.

Anything else besides these 5 requirements are considered optional.

## Optional features

Possible nice to have features would be:

1. Enable the app to distinguish between different users.
2. Enable the app to restrict action for different users.
3. Enable the user to change the Look'n'Feel of the app.

---
title: Plugins - How to apply this concept in C#
tags:
  - Introduction
  - Requirements
thumbnail: /assets/puzzle.png
cover: /assets/puzzle.png
toc: true
category:
  - Plugins in C#
date: 2021-01-04 01:02:05
---



Plugins, a magical buzzword for some people, annoyce for others. Many know how to use them and even how to write them for an existing system. When starting from scratch plugins become quite a bit more difficult to comprehend for the novice.

This article is for all these folks who try for the first time to implement a plugin-system which allows you to extend an application without touching the main application itself.

<!-- more -->

## What is a plugin in anyway?

> A plug-in is a software component which can be added to existing software-system to either extend or change its functionality.

When reading this, questions like:

- How to load an Plugin?
- How to design my software to accept plugins
- How to communicate between Plugin and the main Application?
- How to communicate between Plugins?
- How to abstract a Plugin in a meaning full matter?  

might arise at the start of your journy. These questions and probably a bunch more are being tackled in this series of articles.

## What do you need to follow me?

The requirements are quite low

|Tool/resource|version|source |
|-------------|-------|-------|
| VSCode | latest |[download](https://code.visualstudio.com/download) |
| dotnet | latest |[download](https://dotnet.microsoft.com/download/dotnet/5.0) |
| git | latest | [download](https://git-scm.com/downloads) |
| time |     | You |
| patience | | You |

### Why patience? Well, ...

Upfront i like to apologize. English is not my native language and in either language my grammar sucks spectecular. So be mentally prepared to witness some creative uses of tenses, word order and spelling choices. You are of course welcome to inform me of my mistakes down in the comments.

## Roadmap

What i plan to do is developing an application from scratch. An Application which is designed to load its functionality via external sources.

Each step should basically represent its own article. The following list will be the articles you can expect:

- [Setup a basic solution with a project](../Setup-a-basic-solution-with-a-project)
- Add a new library project to the solution.
- Load an Assembly(DLL) at runtime.
- Define what a PlugIn should look like.
- Load a PlugIn from an Assembly at runtime.
- What is a SDK, and why you need to define your own!
- What will be the main application?

If you have any questions regarding the code please feel free to use the comment section of the article or try to contact me via mail directly.

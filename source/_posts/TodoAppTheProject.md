---
title: The Project - 'What is the purpose of this here?'
toc: true
tags:
  - Introduction
  - SOLID
  - Clean Architecture
cover: /assets/TodoAppMockUp.PNG
thumbnail: /assets/TodoAppMockUp.PNG
categories:
  - TodoApp
date: 2020-10-18 14:11:20
---

# Introduction

Everything, one does try to learn, need at some point a practical example. I want to learn how to apply the Clean Code/Architecture approach from [Robert C. Martin](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)(uncle bob) and [SOLID](https://en.wikipedia.org/wiki/SOLID) applied on an Angular app. The planned app ahead shall be my practical example 😉. The result of this project should represent a todolist-app. Something very basic. Adding a task, removing a task, marking a task as done, where task are simply a textual description coupled with a state.

<!-- more -->

## What is 'Clean Architecture'

The Concept of "Clean Architecture" is quite simple. In essence every outerlayer of an software architecture can use parts of the innerlayers beneath it but never the otherway around. External dependcies of the third party software should be unknown to your innerlayers becauses these kind of depencies might change but your internals should not be affected by this kind of change. This simple rule of having a directed dependency from outer to innerlayer is fundamental to the "Clean Architecture" approach. The question now is how to accomplish this directed dependency in code?

## How is clean Architecture accomplished'

If you are familiar with [SOLID](https://en.wikipedia.org/wiki/SOLID) you might already know the answer, for all who are new the topic let me quickly explain: SOLID is an acronym for five basic design principles every programmer should know. These principles state what kind of questions you need to ask your self while designing a software component. One Of theses design principles is the [Dependency Inversion Principle](https://stackify.com/dependency-inversion-principle/) (DIP). This principles states that:

- High-level modules should not depend on Low-level modules. Both should depend on abstractions.
- Abstractions should not depend on details. Details should depend on abstractions.

What are Details/abstractions you might ask. How does theses terms look like if applied to actual code?
The short answer is abstractions are interfaces with no implementation behind them and details are the implementations of said interfaces, commonly referred to as implementation details. My current understanding is that any component(class/function/module) which has someform of composition happening must ensure that this composition is never dependent on any concrete implementation details but instead rely on abstraction aka interfaces.

```csharp
/*
 Example of a violation from DIP.
 Here you see that the component defines a dependency to some other concrete component
 and tries to build that dependency on its own. something which should be avoided.
*/

class SomeComponent{
    private OtherConcreteComponent _someObject;

    public SomeComponent(){
        this._someObject = new OtherConcreateComponent(); //Bad, class is responsbile to build external dependency
    }
}

class OtherConcreteComponent {}

```

One of the consquences according to my understanding is e.g. that a component should never be responsible of building dependencies on its own but the dependencies must be provided from outside of the component.
If i would try to apply my current understanding to the class above the code would change to something like this:

```csharp
/*
 Example of a applying DIP.
 Here we broke the direct dependency using an interface which represents our previous concreteComponent.
 The OtherConcreateComponent is build by someone else and only the working instance is provided the component
*/
class SomeComponent{
    private IComponent _someObject;

    public SomeComponent(IComponent instance){
        this._someObject = instance; //Good, class is not responsible to build external dependency
    }
}

interface IComponent{}

class OtherConcreteComponent : IComponent {}

```

# What is the purpose of this here

Unfortunatly the given examples are written in C# and not Typescript. And as i stated the project is about an angular app implementing a todolist functionality while at the same time adhereing to clean archtecture and SOLID. I currently **do not know** how this will look like, what abstractions will i use or in general how i will organize the code. Because this lack of knowledge i started this project. To fill this gap in knowldege.

---
title: Setup a basic solution with a project
tags:
  - Introduction
  - Setup
  - csharp
thumbnail: /assets/puzzle.png
cover: /assets/puzzle.png
toc: true
category:
  - Plugins in C#
date: 2021-01-04 01:03:18
---


## Getting Ready

I assume you have VSCode && .Net installed by now. Open VSCode in the directoy of your choice. The directoy you choose is our Solution directory which will contain all our projects.

Your Explorer in VSCode should look like this:
![Empty VSCode Explorer](/assets/EmptyExplorer.PNG)

<!-- more -->

In VSCode use

    Ctrl + Shift + P

and type

    Create new Integrated Terminal


till you can select that option. if you click on it a new terminal will open within VSCode.![Toggle terminal](/assets/CreateNewIntegratedTerminal.PNG) 
In my case the git bash on windows is opened ![see clean && empty terminal](/assets/NewBashJustOpened.PNG)

## Create a Solution

Let's first create a empty solution:
In my case i choose __"/d/Projects/Game"__ as directory for my solution. Check that the newly opened terminal uses the directoy you chose as solution directory. if not change the path with help of the command:

```bash
cd <path to you solution dir>
```

After checking and perhaps adjusting the directory type the following command to create new solution:

```bash
dotnet new sln 
```
Your Explorer in VSCode should look like this afterwards
![Explorer containing only a solution file](/assets/SolutionCreated.PNG)

The directory's name is the name of your solution.

## Create a Project

If done successully create a console application with the following command:

```bash
dotnet new console -n "App" 
```
This creates new console based application and should result in the follwing output in the Explorer
![Explorer containing project](/assets/CreatedNewProject.PNG)

When done you need to add the newly created project to your solution. this is done via the following command:

```bash
dotnet sln add App/App.csproj
```

## Build the Solution && Run the project
After that you can use

```bash
dotnet build #builds all projects which where added to the solution
dotnet run --project App/App.csproj # just as you might guessed it, it runs our console application
```
to test your work. if everything works you should see a HelloWorld on your terminal:
![HelloWorld!](/assets/HelloWorld.PNG)


## Summary

With the five dotnet instruction we have created a simple Hello World solution. which is the starting point of our PluginSystem development.

```bash
dotnet new sln # Creates a new solution with the containing foldername as solution name
dotnet new console -n "App" # creates a new csproj with name App in the sub-dir "App"
dotnet sln add App/App.csproj # adds csproj to solution
dotnet build #builds all projects which where added to the solution
dotnet run --project App/App.csproj # just as you might guessed it, it runs our console application
```
## External Resources

- [VSCode Open Terminal](https://code.visualstudio.com/docs/editor/integrated-terminal#:~:text=To%20open%20the%20terminal%3A,View%3A%20Toggle%20Integrated%20Terminal%20command.)
- [MSDN dotnet sln](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-sln)
- [MSDN dotnet new](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new)
